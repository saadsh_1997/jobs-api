class Post < ApplicationRecord
  # model association
  has_many :job_applications, dependent: :destroy
  # validation
  validates_presence_of :created_by, :title, :body
end

class User < ApplicationRecord
  has_secure_password

  has_many :job_applications, foreign_key: :created_by
  has_many :posts, foreign_key: :created_by

  validates_presence_of :firstname, :lastname, :username, :password_digest, :role
  enum role: { job_seaker: 0, admin: 1 }, _prefix: true
end

class JobApplication < ApplicationRecord
  # model association
  belongs_to :post
  # validation
  validates_presence_of :created_by, :status
end

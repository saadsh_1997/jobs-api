require "rails_helper"

RSpec.describe "JobApplications", type: :request do
  # Initialize the test data
  let(:user) { create(:user, role: :job_seaker) }
  let!(:post_record) { create(:post, created_by: user.id) }
  let!(:job_applications) { create_list(:job_application, 20, post_id: post_record.id) }
  let(:post_id) { post_record.id }
  let(:id) { job_applications.first.id }
  let(:headers) { valid_headers }

  # Test suite for GET /posts/:post_id/job_applications
  describe "GET /posts/:post_id/job_applications" do
    before { get "/posts/#{post_id}/job_applications", params: {}, headers: headers }

    context "when post exists" do
      it "returns status code 200" do
        expect(response).to have_http_status(200)
      end

      it "returns all post job_applications" do
        expect(json.size).to eq(20)
      end
    end

    context "when post does not exist" do
      let(:post_id) { 0 }

      it "returns status code 404" do
        expect(response).to have_http_status(404)
      end

      it "returns a not found message" do
        expect(response.body).to match(/Couldn't find Post/)
      end
    end
  end

  # Test suite for GET /posts/:post_id/job_applications/:id
  describe "GET /posts/:post_id/job_applications/:id" do
    before { get "/posts/#{post_id}/job_applications/#{id}", params: {}, headers: headers }

    context "when post job_application exists" do
      it "returns status code 200" do
        expect(response).to have_http_status(200)
      end

      it "returns the job_application" do
        expect(json["id"]).to eq(id)
      end
    end

    context "when post job_application does not exist" do
      let(:id) { 0 }

      it "returns status code 404" do
        expect(response).to have_http_status(404)
      end

      it "returns a not found message" do
        expect(response.body).to match(/Couldn't find JobApplication/)
      end
    end
  end

  # Test suite for PUT /posts/:post_id/job_applications
  describe "POST /posts/:post_id/job_applications" do
    let(:valid_attributes) { { status: 0, created_by: "123" }.to_json }

    context "when request attributes are valid" do
      before { post "/posts/#{post_id}/job_applications", params: valid_attributes, headers: headers }

      it "returns status code 201" do
        expect(response).to have_http_status(201)
      end
    end

    context "when an invalid request" do
      before { post "/posts/#{post_id}/job_applications", params: { created_by: "123" }.to_json, headers: headers }

      it "returns status code 422" do
        expect(response).to have_http_status(422)
      end

      it "returns a failure message" do
        expect(response.body).to match(/Validation failed: Status can't be blank/)
      end
    end
  end

  # Test suite for PUT /posts/:post_id/job_applications/:id
  describe "PUT /posts/:post_id/job_applications/:id" do
    let(:valid_attributes) { { status: 1 }.to_json }

    before { put "/posts/#{post_id}/job_applications/#{id}", params: valid_attributes, headers: headers }

    context "when job_application exists" do
      it "returns status code 204" do
        expect(response).to have_http_status(204)
      end

      it "updates the job_application" do
        updated_job_application = JobApplication.find(id)
        expect(updated_job_application.status).to eq(1)
      end
    end

    context "when the job_application does not exist" do
      let(:id) { 0 }

      it "returns status code 404" do
        expect(response).to have_http_status(404)
      end

      it "returns a not found message" do
        expect(response.body).to match(/Couldn't find JobApplication/)
      end
    end
  end

  # Test suite for DELETE /posts/:id
  describe "DELETE /posts/:id" do
    before { delete "/posts/#{post_id}/job_applications/#{id}", params: {}, headers: headers }

    it "returns status code 204" do
      expect(response).to have_http_status(204)
    end
  end
end

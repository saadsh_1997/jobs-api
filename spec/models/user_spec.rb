require "rails_helper"

RSpec.describe User, type: :model do
  it { should have_many(:posts) }
  it { should have_many(:job_applications) }
  it { should validate_presence_of(:firstname) }
  it { should validate_presence_of(:lastname) }
  it { should validate_presence_of(:username) }
  it { should validate_presence_of(:password_digest) }
  it { should validate_presence_of(:role) }
end

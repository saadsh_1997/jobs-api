require "rails_helper"
RSpec.describe JobApplicationPolicy, type: :policy do
  let(:admin) { create(:user, role: :admin) }
  let(:job_seaker) { create(:user, role: :job_seaker) }
  let!(:job_post) { create(:post, created_by: admin.id) }
  let(:job_application) { create(:job_application, post_id: job_post.id) }
  subject { described_class }

  permissions :index? do
    it "Job applications requested" do
      expect(subject).to permit(nil, job_application)
    end
  end
  permissions :show? do
    it "Job applications requested" do
      expect(subject).to permit(nil, job_application)
    end
  end
  permissions :create? do
    it "Job Seeker" do
      expect(subject).to permit(job_seaker, job_application)
    end
    it "Admin User" do
      expect(subject).not_to permit(admin, job_application)
    end
  end
  permissions :update? do
    it "Job Seeker" do
      expect(subject).to permit(job_seaker, job_application)
    end
    it "Admin User" do
      expect(subject).not_to permit(admin, job_application)
    end
  end
  permissions :destroy? do
    it "Job Seeker" do
      expect(subject).to permit(job_seaker, job_application)
    end
    it "Admin User" do
      expect(subject).not_to permit(admin, job_application)
    end
  end
end

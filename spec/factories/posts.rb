FactoryBot.define do
  factory :post do
    title { Faker::Job.title }
    body { Faker::Lorem.paragraph }
    created_by { Faker::Number.number(digits: 10) }
  end
end

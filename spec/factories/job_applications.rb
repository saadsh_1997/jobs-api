FactoryBot.define do
  factory :job_application do
    status { Faker::Number.between(from: 0, to: 1) }
    created_by { Faker::Number.number(digits: 10) }
    post_id { nil }
  end
end
